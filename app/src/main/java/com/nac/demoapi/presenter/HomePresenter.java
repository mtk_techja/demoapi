package com.nac.demoapi.presenter;

import android.util.Log;

import com.nac.demoapi.api.IEmployeeAPI;
import com.nac.demoapi.entities.ListEmployeeEntity;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomePresenter {
    private static final String BASE_URL = "http://dummy.restapiexample.com/api/v1/";

    private static final String TAG = HomePresenter.class.getName();
    private IEmployeeAPI mEmployeeAPI;

    public HomePresenter() {
        initAPI();
    }

    private void initAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder()
                        .callTimeout(30, TimeUnit.SECONDS)
                        .hostnameVerifier((hostname, session) -> true)
                        .build())
                .build();

        //Tạo request từ interface
        mEmployeeAPI = retrofit.create(IEmployeeAPI.class);
    }


    public Response<ListEmployeeEntity> getAllEmployee() {
        Log.i(TAG, "getAllEmployee...");
        try {
            return mEmployeeAPI.getAllEmployee().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
