package com.nac.demoapi;

public interface OnActionCallBack<T> {
    void callBack(int key, T data);
}
