package com.nac.demoapi.entities;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListEmployeeEntity extends BaseDataEntity {
    @SerializedName("data")
    private List<EmployeeEntity> listEmployee;

    public List<EmployeeEntity> getListEmployee() {
        return listEmployee;
    }

    public void setListEmployee(List<EmployeeEntity> listEmployee) {
        this.listEmployee = listEmployee;
    }

    @NonNull
    @Override
    public String toString() {
        return getStatus() + "\n" + listEmployee.toString();
    }
}
